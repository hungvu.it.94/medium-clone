const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let articleSchema = new Schema({
  text: String,
  title: String,
  description: String,
  claps: Number
});

module.exports = mongoose.model('Article', articleSchema);
